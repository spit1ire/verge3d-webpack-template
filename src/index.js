import * as v3d from './v3d.module';
import './styles/styles.css';

// Simple template;

class Preload {

  constructor(update = ()=>{}, finish = ()=>{}){
    this.update = update;
    this.finish = finish;
  }

  onUpdate (e){
    this.update(e)
  };
  onFinish(e){
    this.finish(e)
  };
}

window.addEventListener('load', function () {

  const container = document.querySelector('#v3d-container')

  function onUpdate(prog) {
    console.log(prog);
  }

  function onFinish(params) {
    console.log(params);
    console.log('finish');
  }
  var app = new v3d.App('v3d-container', null,
  new Preload(onUpdate, onFinish)
  );

  app.renderer.setPixelRatio( window.devicePixelRatio );
  app.renderer.setSize( window.innerWidth, window.innerHeight );

  window.addEventListener( 'resize', onWindowResize, false );

  function onWindowResize() {

    app.camera.aspect = window.innerWidth / window.innerHeight;
    app.camera.updateProjectionMatrix();

    app.renderer.setSize( window.innerWidth, window.innerHeight );

  }


  const url = './assets/f.gltf';

  app.loadScene(url, function () {
    app.enableControls();
    app.run();
    runCode();
  });

  function runCode() {
    // add your code here, e.g. console.log('Hello, World!');

  }

});